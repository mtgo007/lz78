#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <cstring>
#include <stdlib.h>

// Troca a extensao de um arquivo
// arquivo.txt -> arquivo.z78
// arquivo.z78 -> arquivo.txt
void ChangeExtension(char* &nome, std::string extension);

#endif