#ifndef DICTIONARY_CELL_H
#define DICTIONARY_CELL_H

struct DictionaryCell {
    // Índice da célula pai no vetor de objetos do tipo DictionaryCell
    unsigned int code;
    // Caractere armazenado pela célula do dicionário
    char data;
};

#endif