#ifndef NODE_H
#define NODE_H

#include <vector>

class Node {
    public:
        // Construtor para os nós
        Node();
        // Destrutor do nó
        ~Node();

        // Retorna se o nó possui um filho associado ao caractere
        bool haveBranch(Node* &child, char c);

    private:
        // Ponteiro para o pai do nó
        Node* father;
        // Vetor de ponteiro para os filhos
        std::vector<Node*> childs;
        // Caractere que será armazenado no nó
        char data;
        // Código da string armazenada
        unsigned int code;


        friend class Trie;
};

#endif