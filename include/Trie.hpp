#ifndef TRIE_H
#define TRIE_H

#include "Node.hpp"
#include <stdlib.h>
#include <stdio.h>

#define NUM_CHAR_READEN 1

// Essa árvore Trie será especializada para realizar a compressão
class Trie {
    public:
        // Construtor da árvore
        Trie(FILE* _textoFile, FILE* _compressaoFile);
        // Destrutor da árvore
        ~Trie();

        // Método que realiza a compressão
        void Lz78();

    private:
        // Raiz da árvore
        Node* root;
        // Arquivos que serão manipulados pelo algoritmo de compressao
        FILE* textoFile;
        FILE* compressaoFile;

        // Método que insere um novo nó na árvore
        void Insert(Node* father, char nodeData, int code);
};

#endif