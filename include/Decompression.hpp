#ifndef DESCOMPRESSAO_H
#define DESCOMPRESSAO_H

#include <stdio.h>
#include <string>
#include <vector>
#include "DictionaryCell.hpp"

#define STRING_START 0
#define NUMBER_ELEMENTS_TO_READ 1

// Procedimento que descomprime um arquivo
void Lz78Descompression(FILE* compressedFile, FILE* decompressedFile);

// Procedimento que gera a string representada por uma posição
// do dicionário
void PrintString(std::vector<DictionaryCell> &dictionary, 
                       FILE* &decompressedFile, unsigned int code);

#endif