#include "Decompression.hpp"

void Lz78Descompression(FILE* compressedFile, FILE* decompressedFile) {
    int associatedCode;
    char data;
    unsigned int elementIndex = 0;
    std::vector<DictionaryCell> dictionary;
    DictionaryCell item;

    // Lemos o arquivo comprimido
    // Lemos um código e um caractere
    while(fread(&associatedCode, sizeof(int), NUMBER_ELEMENTS_TO_READ, compressedFile) == NUMBER_ELEMENTS_TO_READ
          && fread(&data, sizeof(char), NUMBER_ELEMENTS_TO_READ, compressedFile) == NUMBER_ELEMENTS_TO_READ) {

        // Configuramos a nova célula do dicionário
        item.code = associatedCode;
        item.data = data;

        // Adicionamos a nova célula ao dicionário
        dictionary.push_back(item);

        // Imprimimos a string correspondente
        PrintString(dictionary, decompressedFile, elementIndex);

        // Incrementamos a variável que armazena o índice
        // do próximo elemento que será inserido no dicionário
        elementIndex++;
    }

}

void PrintString(std::vector<DictionaryCell> &dictionary, 
                       FILE* &decompressedFile, unsigned int code) {

    // Passo Base
    // Caso seja o nó raiz, nada é impresso.
    // Isso simula o comportamento de imprimir a string vazia no arquivo de texto.
    if(code == STRING_START)
        return;

    // Passo Recursivo
    // Chamamos o procedimento recursivamente para a célula pai 
    // da célula passada com parâmetro.
    PrintString(dictionary, decompressedFile, dictionary[code].code);
    // Escrevemos o caractere da célula no arquivo de texto
    fprintf(decompressedFile, "%c", dictionary[code].data);
}