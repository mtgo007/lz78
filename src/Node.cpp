#include "Node.hpp"

Node::Node() {
    // Configurações padrões (Correspondem ao nó raiz)
    // Como não existe um caractere correspondente a string vazia,
    // o caractere armazenado pelo nó raiz é um *
    data = '*';
    code = 0;
    father = nullptr;
}

// Será necessário um caminhamento pós ordem para 
// dealocar a memória ocupada pelo nó e por seus filhos
Node::~Node() {
    std::vector<Node*>::size_type i;
    
    // Desalocamos a memória ocupada por cada um dos filhos
    for(i=0; i<childs.size(); i++) {
        delete childs[i];
    }
}

bool Node::haveBranch(Node* &child, char c) {
    std::vector<Node*>::size_type i;

    for(i=0; i<childs.size(); i++) {
        if(childs[i]->data == c) {
            child = childs[i];
            return true;
        }
    }
    return false;
}