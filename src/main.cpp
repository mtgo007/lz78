#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>
#include "Utils.hpp"
#include "Trie.hpp"
#include "Decompression.hpp"

#define MIN_PARAM 3
#define COMPRESSION 0
#define DECOMPRESSION 1
#define PARAM_NOME 2
#define PARAM_TARGET 4
#define OPERATION_PARAM 1
#define STRINGS_IGUAIS 0
#define EXECUTION_FAILURE -1

int main(int argc, char** argv) {
    // Vamos assumir que a entrada será bem comportada
    // Ou seja, assumimos que os parâmetros estarão dentro do padrão especificado
    int operation;
    char* nomeSource;
    char* nomeTarget;
    
    // Variáveis de leitura
    FILE* sourceFile;
    FILE* targetFile;

    // Dicionário para Compressão
    Trie* trie;

    // Variáveis auxiliares
    int sourceLength;

    // Instanciamos o nome do arquivo de entrada
    nomeSource = argv[PARAM_NOME];

    // Verificamos qual operacao sera executada
    if(strcmp(argv[OPERATION_PARAM], "-c") == STRINGS_IGUAIS) {
        operation = COMPRESSION;
    } else {
        operation = DECOMPRESSION;
    }

    // Verificamos se o arquivo de saída foi especificado
    if(argc > MIN_PARAM) {
        // Nesse caso o arquivo de saída foi especificado
        nomeTarget = argv[PARAM_TARGET];
    } else {
        // Nesse caso o arquivo de saída não foi especificado
        sourceLength = strlen(nomeSource);
        // Alocamos um vetor para armazenar o nome do arquivo de saída.
        // Adiciona-se uma posicão extra para o caractere '\0'
        nomeTarget = new char[sourceLength + 1];
        // Copiamos o nome do arquivo de entrada para o vetor que
        // armazena o nome do arquivo de saída
        strcpy(nomeTarget, nomeSource);
        // O nome de destino depende da operacao que será realizada
        if(operation == COMPRESSION) {
            ChangeExtension(nomeTarget, "z78");
        } else {
            ChangeExtension(nomeTarget, "txt");
        }
    }

    // Abrimos os dois arquivos
    sourceFile = fopen(nomeSource, "r");
    targetFile = fopen(nomeTarget, "w");

    // Verificamos se os arquivos foram abertos com sucesso
    if(sourceFile == NULL || targetFile == NULL) {
        printf("Erro ao abrir arquivos\n");
        return EXECUTION_FAILURE;
    }

    // Caso a operação seja de compressão
    if(operation == COMPRESSION) {
        // Construímos a árvore
        trie = new Trie(sourceFile, targetFile);
        // Comprimimos o arquivo
        trie->Lz78();
        // Desalocamos a árvore
        delete trie;
    } 
    // Caso a operação seja de descompressão
    else {
        // Descomprimimos o arquivo
        Lz78Descompression(sourceFile, targetFile);
    }

    // Liberamos os arquivos
    fclose(sourceFile);
    fclose(targetFile);


    // Liberamos o vetor que armazena o nome do arquivo de saída
    //  caso ele tenha sido alocado
    if(argc <= MIN_PARAM) {
        delete[] nomeTarget;
    }

}