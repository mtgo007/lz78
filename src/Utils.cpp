#include "Utils.hpp"

void ChangeExtension(char* &nome, std::string extension) {
    int strIndex, extIndex;
    int nomeLength = strlen(nome);
    int extensionLength = extension.size();

    for(strIndex = nomeLength - extensionLength, extIndex = 0; 
            strIndex < nomeLength; strIndex++, extIndex++) {
        nome[strIndex] = extension[extIndex];
    }
}
