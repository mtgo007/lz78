#include "Trie.hpp"

Trie::Trie(FILE* _textoFile, FILE* _compressaoFile) {
    // Alocamos o nó raiz
    root = new Node();

    // Armazenamos o arquivo que será comprimido
    textoFile = _textoFile;
    compressaoFile = _compressaoFile;
}

Trie::~Trie() {
    // A desalocacão dos arquivos não é feita no destrutor da árvore
    // Desalocamos os nós
    delete root;
}

void Trie::Insert(Node* father, char nodeData, int code) {
    // Alocamos um novo nó
    Node *newNode = new Node();

    // Configuramos o novo nó
    newNode->code = code;
    newNode->data = nodeData;
    newNode->father = father;

    // Adicionamos o nó à árvore
    father->childs.push_back(newNode);
}


void Trie::Lz78() {
    // Variáveis auxiliares
    Node* insertionNode = root;
    char charReaded;
    bool matched = false;
    int nextCode = 1;
    bool wasCharReaded = false;

    // Escrevemos no arquivo comprimido o conteúdo correspondente ao nó raiz
    fwrite(&(root->code), /*Endereço em que estão os dados que serão escritos*/
            sizeof(char), /*Tamanho dos blocos de dados que serão escritos*/
            sizeof(int),  /*Número de blocos de dados serão escritos*/
            compressaoFile /*Arquivo que será escrito*/);
    fwrite(&(root->data), sizeof(char), sizeof(char), compressaoFile);

    // Iteramos por todos os caracteres do texto
    // Lemos um caractere
    while(fscanf(textoFile, "%c", &charReaded) == NUM_CHAR_READEN) {
        // Sinalizamos que um caractere foi lido
        wasCharReaded = true;
        // Verificamos se existe um caminho na árvore que representa
        // os caracteres lidos
        matched = insertionNode->haveBranch(insertionNode, charReaded);

        if(matched) {
            // Caso a sequência de caracteres lidos esteja na árvore
            // Continuamos a leitura
            continue;
        }
            
        // Caso a sequência de caracteres lidos não esteja na árvore
        // É necessário inserir na árvore o último caractere lido como
        // filho de insertionNode
        Insert(insertionNode, charReaded, nextCode);
        // Atualizamos a variável que controla o código dos nós que serão inseridos na árvore
        nextCode++;
        // Imprimimos a representação da substring processada do texto original 
        // no arquivo comprimido
        fwrite(&(insertionNode->code), sizeof(char), sizeof(int), compressaoFile);
        fwrite(&charReaded, sizeof(char), sizeof(char), compressaoFile);
        // Atualizamos o nó de inserção, pois, a próxima substring do texto original
        // será procurada na árvore a partir do nó raiz
        insertionNode = root;
        // Sinalizamos que a substring que será processada ainda 
        // não possui nenhum caractere
        wasCharReaded = false;
    }
    // É possível que o último sufixo do texto original tivesse sido previamente
    // inserido na árvore. Nesse caso, o loop acima não escreve a representação
    // correspondente ao sufixo final do texto no arquivo comprimido.
    // Esse caso deve ser tratado separadamente.
    if(wasCharReaded) {
        // Imprimimos o código do pai do nó que corresponde ao último caractere lido
        fwrite(&(insertionNode->father->code), sizeof(char), sizeof(int), compressaoFile);
        // Imprimimos o último caractere lido
        fwrite(&charReaded, sizeof(char), sizeof(char), compressaoFile);
    }
}