# Configuracoes do compilador
CC = g++
CFLAGS = -std=c++11 -Wall
PROGRAM = tp1.out

# Variáveis auxiliares
BUILD_DIR = ./build
INCLUDE_DIR = ./include
SRC_DIR = ./src

# Regras de Compilacao
${PROGRAM}: Utils.o Node.o Trie.o Decompression.o
	${CC} ${CFLAGS} -o ${PROGRAM} ${SRC_DIR}/main.cpp -I ${INCLUDE_DIR} ${BUILD_DIR}/*.o 

Utils.o:
	${CC} ${CFLAGS} -o ${BUILD_DIR}/Utils.o -c ${SRC_DIR}/Utils.cpp -I ${INCLUDE_DIR}

Node.o:
	${CC} ${CFLAGS} -o ${BUILD_DIR}/Node.o -c ${SRC_DIR}/Node.cpp -I ${INCLUDE_DIR}

Trie.o:
	${CC} ${CFLAGS} -o ${BUILD_DIR}/Trie.o -c ${SRC_DIR}/Trie.cpp -I ${INCLUDE_DIR}

Decompression.o:
	${CC} ${CFLAGS} -o ${BUILD_DIR}/Decompression.o -c ${SRC_DIR}/Decompression.cpp -I ${INCLUDE_DIR}

clean:
	rm -rf ${PROGRAM}
	rm -rf ${BUILD_DIR}/*.o